
var fs             =  require('fs');
var express        =  require('express');
var bodyParser     =  require("body-parser");
var controller     =  require('./app/controllers/controller');
var serverConfig   =  JSON.parse(fs.readFileSync('./config/server.json'));

var app  =  express();

app.use(bodyParser.json());

app.post('/send-message', controller.use('messages').send);

app.get('/countries', controller.use('countries').load);




var server = app.listen(serverConfig.port, function(){

	console.log('We are now listening on %s:%s', server.address().address, server.address().port );
});
