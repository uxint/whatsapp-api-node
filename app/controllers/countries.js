var fs       = require('fs');

function load(req, res){

	var  countries = JSON.parse(fs.readFileSync('raw_data/countries.json'));

	res.setHeader("Content-Type", "text/json");

	res.send( countries , 200);
}



module.exports.load = load;
