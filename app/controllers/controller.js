function use(name){
	var controllerFile = './' + name;
	return require(controllerFile);
}

module.exports.use  = use;