var fs           =  require('fs');
var whatsapi     =  require('whatsapi');
var bodyParser   =  require("body-parser");

function send(req, res){

	var  whatsAppCreds = JSON.parse(fs.readFileSync('config/whatsApp.json'));
	var  wa = whatsapi.createAdapter(whatsAppCreds);

	wa.connect(function connected(err) {
	    	
		    	if(err) {  console.log(err); return; }

			    console.log('Connected');
			    // Now login
			    wa.login(function(err) {

				    if (err) { console.log(err); return; }
				    console.log('Logged in to WA server');
				    wa.sendIsOnline();

					 var recipient     = req.body.msisdn;
					 var messageText   = req.body.message_text;

					// console.log(messageText);

				    if(recipient && messageText )
				    {
						wa.sendMessage(recipient, messageText, function(err, id) {
						    if (err) { console.log(err.message); return; }
						    console.log('Server received message %s', id);
						});
				    }
				});
	});

	res.setHeader("Content-Type", "text/json");
	res.send('Message is being sent', 200);

}



module.exports.send = send;
